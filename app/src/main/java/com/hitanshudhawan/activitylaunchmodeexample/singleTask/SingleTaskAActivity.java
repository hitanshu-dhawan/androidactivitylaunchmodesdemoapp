package com.hitanshudhawan.activitylaunchmodeexample.singleTask;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.hitanshudhawan.activitylaunchmodeexample.R;

/**
 * A -> B -> C -> B == A -> B
 * In this launch mode a new task will always be created and a new instance will be pushed to the task as the root one.
 * If an instance of activity exists on the separate task,
 * a new instance will not be created and Android system routes the intent information through onNewIntent() method.
 * At a time only one instance of activity will exist.
 */

public class SingleTaskAActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_task_a);

        findViewById(R.id.alphabet_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SingleTaskAActivity.this, SingleTaskBActivity.class));
            }
        });
    }
}
