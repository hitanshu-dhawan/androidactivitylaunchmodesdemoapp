package com.hitanshudhawan.activitylaunchmodeexample.singleTop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.hitanshudhawan.activitylaunchmodeexample.R;

/**
 * A -> B -> B -> B
 * In this launch mode if an instance of activity already exists at the top of the current task,
 * a new instance will not be created and Android system will route the intent information through onNewIntent().
 * If an instance is not present on top of task then new instance will be created.
 */

public class SingleTopAActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_top_a);

        findViewById(R.id.alphabet_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SingleTopAActivity.this, SingleTopBActivity.class));
            }
        });
    }
}
