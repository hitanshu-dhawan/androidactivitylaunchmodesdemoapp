package com.hitanshudhawan.activitylaunchmodeexample.singleTop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.hitanshudhawan.activitylaunchmodeexample.R;

public class SingleTopBActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_top_b);

        findViewById(R.id.alphabet_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SingleTopBActivity.this, SingleTopBActivity.class));
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Toast.makeText(this, "onNewIntent", Toast.LENGTH_SHORT).show();
    }
}
