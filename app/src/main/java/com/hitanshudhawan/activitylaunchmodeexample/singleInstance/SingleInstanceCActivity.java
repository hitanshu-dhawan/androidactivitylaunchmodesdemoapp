package com.hitanshudhawan.activitylaunchmodeexample.singleInstance;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hitanshudhawan.activitylaunchmodeexample.R;

public class SingleInstanceCActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_instance_c);
    }
}
