package com.hitanshudhawan.activitylaunchmodeexample.singleInstance;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.hitanshudhawan.activitylaunchmodeexample.R;

/**
 * A -> B -> C
 * This is very special launch mode and only used in the applications that has only one activity.
 * It is similar to singleTask except that no other activities will be created in the same task.
 * Any other activity started from here will create in a new task.
 */

public class SingleInstanceAActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_instance_a);

        findViewById(R.id.alphabet_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SingleInstanceAActivity.this, SingleInstanceBActivity.class));
            }
        });
    }
}
