package com.hitanshudhawan.activitylaunchmodeexample.singleInstance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.hitanshudhawan.activitylaunchmodeexample.R;

public class SingleInstanceBActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_instance_b);

        findViewById(R.id.alphabet_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SingleInstanceBActivity.this, SingleInstanceCActivity.class));
            }
        });
    }
}
