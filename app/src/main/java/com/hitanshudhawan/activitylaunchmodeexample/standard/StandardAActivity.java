package com.hitanshudhawan.activitylaunchmodeexample.standard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.hitanshudhawan.activitylaunchmodeexample.R;

/**
 * A -> B -> C -> B -> C
 * This is the default launch mode of an activity (If not specified).
 * It creates a new instance of an activity in the task from which it was started.
 * Multiple instances of the activity can be created and multiple instances can be added to the same or different tasks.
 * In other words you can create the same activity multiple times in the same task as well as in different tasks.
 */

public class StandardAActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standard_a);

        findViewById(R.id.alphabet_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StandardAActivity.this, StandardBActivity.class));
            }
        });
    }
}
