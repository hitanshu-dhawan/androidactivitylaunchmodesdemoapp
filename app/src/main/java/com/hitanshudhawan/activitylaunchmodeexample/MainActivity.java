package com.hitanshudhawan.activitylaunchmodeexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.hitanshudhawan.activitylaunchmodeexample.singleInstance.SingleInstanceAActivity;
import com.hitanshudhawan.activitylaunchmodeexample.singleTask.SingleTaskAActivity;
import com.hitanshudhawan.activitylaunchmodeexample.singleTop.SingleTopAActivity;
import com.hitanshudhawan.activitylaunchmodeexample.standard.StandardAActivity;

/**
 * Activity Launch Modes
 * <p>
 * standard
 * {@link com.hitanshudhawan.activitylaunchmodeexample.standard.StandardAActivity}
 * <p>
 * singleTop
 * {@link com.hitanshudhawan.activitylaunchmodeexample.singleTop.SingleTopAActivity}
 * <p>
 * singleTask
 * {@link com.hitanshudhawan.activitylaunchmodeexample.singleTask.SingleTaskAActivity}
 * <p>
 * singleInstance
 * {@link com.hitanshudhawan.activitylaunchmodeexample.singleInstance.SingleInstanceAActivity}
 */

/**
 * Intent Flags
 * <p>
 * FLAG_ACTIVITY_NEW_TASK
 * This flag works similar to “launchMode = singleTask”.
 * <p>
 * FLAG_ACTIVITY_CLEAR_TASK
 * This flag will cause any existing task that would be associated with the activity to be cleared before the activity is started.
 * The activity becomes the new root of an otherwise empty task, and any old activities are finished.
 * <p>
 * FLAG_ACTIVITY_SINGLE_TOP
 * This flag works similar to “launchMode = singleTop”.
 * <p>
 * FLAG_ACTIVITY_CLEAR_TOP
 * If set, and the activity being launched is already running in the current task,
 * then instead of launching a new instance of that activity, all of the other activities on top of it will be closed
 * and this Intent will be delivered to the (now on top) old activity as a new Intent.
 */

// Links
// https://android.jlelse.eu/android-activity-launch-mode-e0df1aa72242
// https://developer.android.com/guide/components/activities/tasks-and-back-stack
// https://inthecheesefactory.com/blog/understand-android-activity-launchmode/en

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button01).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, StandardAActivity.class));
            }
        });
        findViewById(R.id.button02).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SingleTopAActivity.class));
            }
        });
        findViewById(R.id.button03).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SingleTaskAActivity.class));
            }
        });
        findViewById(R.id.button04).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SingleInstanceAActivity.class));
            }
        });
    }
}
